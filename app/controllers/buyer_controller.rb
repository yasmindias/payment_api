class BuyerController < ApplicationController
  before_action :set_buyer, only: [:show, :destroy]

  def index
    buyers = Buyer.order('created_at DESC').all
    render json: buyers, status: :ok
  end

  def create
    @buyer = Buyer.new(buyer_params)
    if @buyer.save
      render json: @buyer, status: :ok
    else
      render json: @buyer.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @buyer, status: :ok
  end

  def destroy
    if @buyer.destroy
      render json: {"message":"Deleted", "id":@buyer.id}, status: :ok
    else
      render json: @buyer.errors, status: :unprocessable_entity
    end
  end

  private
  def set_buyer
    @buyer = Buyer.find(params[:id])
  end

  def buyer_params
    params.require(:buyer).permit(:name, :email, :cpf)
  end
end