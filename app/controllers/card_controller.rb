class CardController  < ApplicationController
  before_action :set_card, only: [:show, :destroy]

  def index
    cards = Cards.order('created_at DESC').all
    render json: cards, status: :ok
  end

  def create
    @card = Card.new(card_params)
    if @card.save
      render json: @card, status: :ok
    else
      render json: @card.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @card, status: :ok
  end

  def destroy
    if @card.destroy
      render json: {"message":"Deleted", "id":@card .id}, status: :ok
    else
      render json: @card.errors, status: :unprocessable_entity
    end
  end

  private

  def set_card
    @card = Card.find(params[:id])
  end

  def card_params
    params.require(:card).permit(:holder_name, :number, :exp_date, :cvv)
  end
end