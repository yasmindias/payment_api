class ClientController < ApplicationController
  before_action :set_client, only: [:show]

  def index
    clients = Client.order('created_at DESC').all
    render json: clients, status: :ok
  end

  def create
    @client = Client.new(client_params)
    if @client.save
      render json: @client, status: :ok
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  def show
    render json: @client, status: :ok
  end

  private
  def set_client
    @client = Client.find(params[:id])
  end

  def client_params
    params.require(:client).permit(:name, :cpfcnpj)
  end
end