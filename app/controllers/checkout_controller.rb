class CheckoutController < ApplicationController
  def create
    checkoutservice = CheckoutService.new(checkout_params)

    @response = checkoutservice.createPayment

    if @response[:error]
      render json: @response[:message], status: :unprocessable_entity
    else
      render json: @response[:message], status: :ok
    end
  end

  def checkout_params
    params.permit!
  end
end