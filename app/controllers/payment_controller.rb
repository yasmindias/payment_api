class PaymentController < ApplicationController
  before_action :set_payment, only: [:show, :destroy]

  def index
    payments = Payment.order('created_at DESC').all
    render json: payments, status: :ok
  end

  def create
    payment_service = PaymentService.new(payment_params)
    @response = payment_service.newPayment

    if @response[:error]
      render json: @response[:message], status: :unprocessable_entity
    else
      render json: @response[:message], status: :ok
    end
  end

  def show
    render json: @payment, status: :ok
  end

  def destroy
    if @payment.destroy
      render json: {"message":"Deleted", "id":@payment.id}, status: :ok
    else
      render json: @payment.errors, status: :unprocessable_entity
    end
  end

  private

  def payment_params
    params.require(:payment).permit(:id, :amount, :payment_type, :status, :card_id, :buyer_id)
  end

  def set_payment
    @payment = Payment.find(params[:id])
  end

end