class Payment < ApplicationRecord
  validates :amount, presence: true
  validates :payment_type, presence: true
  validates :card_id, presence: true, :if => :isCreditCard?

  def isCreditCard?
    self.payment_type.eql? "C"
  end
end
