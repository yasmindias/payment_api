class Client < ApplicationRecord
  validates :name, length: {minimum: 1, maximum: 100}
  validates :cpfcnpj, length: {minimum: 11, maximum: 14}, numericality: {only_integer: true}
end