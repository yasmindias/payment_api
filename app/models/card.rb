class Card < ApplicationRecord
  before_save :setCardIssuer

  include ActiveModel::Validations

  EXPDATE_REGEX = /\b(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})\b/

  validates :holder_name, length: {minimum: 1, maximum:100}
  validates :number, length: {minimum:16, maximum:16}, numericality: {only_integer: true}, credit_card_number: true
  validates :exp_date, length: {minimum:5, maximum:5}, format: { with: EXPDATE_REGEX }
  validates :cvv, length: {minimum:3, maximum:3}, numericality: {only_integer: true}

  def setCardIssuer
      detector = CreditCardValidations::Detector.new(self.number)
      if detector.valid?
        self.card_issuer = detector.brand
      end
  end
end