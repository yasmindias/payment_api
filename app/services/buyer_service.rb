class BuyerService

  def initialize(buyer_info)
    @buyer_params = buyer_info
  end

  def getBuyer
    @buyer = Buyer.where("name = ?", @buyer_params[:name]).take
    if @buyer.blank?
      @buyer = Buyer.create!(@buyer_params)
    end
    @buyer
  end
end