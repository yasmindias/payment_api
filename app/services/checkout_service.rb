class CheckoutService
    def initialize(checkout_params){
        @checkout_params = checkout_params
    }

    def checkBuyer
        $buyer = BuyerService.new(@checkout_params[:buyer_info]).getBuyer
    end

    def checkCard
        if checkout_params[:payment_info][:payment_type] == "C"
            $card = CardService.new(@checkout_params[:card_info]).getCard
        end
    end

    def createPayment
        checkBuyer
        checkCard

        payment_service = PaymentService.new(@checkout_params[:payment_info])
        payment_service.newPayment
    end

end 