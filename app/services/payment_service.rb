class PaymentService
  def initialize(payment_params)
    @payment_params = payment_params
  end

  def newPayment
    @payment = Payment.new(@payment_params)
    if paymentIsValid
      setForeignKeys

      if @payment.payment_type == "C"
        @payment.status = "SUCCESSFUL"

        @response = {"status": @payment.status}
      elsif @payment.payment_type == "B"
          @payment.boleto_number = "1233487327489382987349827438932"
          @payment.status = "SUCCESSFUL"

          @response = {"boleto_number": @payment.boleto_number}
      end

      if @payment.save
        @response = {"message":@response}
      else
        @response = {"error":true, "message":@payment.errors}
      end
    else
      @response = {"error":true, "message":{"payment_type": ["Only accepts Credit Card (value: C) or Boleto (value: B)"]}}
    end
  end

  private

  def paymentIsValid
    @payment.payment_type == "C" || @payment.payment_type == "B"
  end

  def setForeignKeys
    if !$buyer.blank?
      @payment.buyer_id = $buyer.id
    end

    if@payment_params[:payment_type] == "C" && !$card.blank?
      @payment.card_id  = $card.id
    end
  end
end