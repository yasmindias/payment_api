require 'credit_card_validations/string'

class CardService
  def initialize(card_info)
    @card_params = card_info
  end

  def getCard
    @card = Card.where(number: @card_params[:number]).take
    if @card.blank?
      @card = Card.create!(@card_params)
    end
    @card
  end
end