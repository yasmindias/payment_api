Rails.application.routes.draw do
  resources :client, :payment, :card, :buyer
  resources :checkout, :only => [:create]

end
