require 'test_helper'

class BuyerTest < ActiveSupport::TestCase
    def setup
        @buyer = Buyer.new(name: "Yasmin Dias", email:"yasmindias.info@gmail.com", cpf: "12345678992")
    end

    test "buyer should be valid" do
        valid = @buyer.valid?
        assert valid, "#{@buyer.errors.as_json}cle"
    end

    test "buyer should be invalid because name is empty" do
        @buyer.name = nil
        assert_not @buyer.valid?
    end

    test "buyer should be invalid because name is too long" do
        @buyer.name = "a"*150
        assert_not @buyer.valid?
    end

    test "buyer should be invalid because email is empty" do
        @buyer.email = nil
        assert_not @buyer.valid?
    end

    test "buyer should be invalid because email is in the wrong format" do
        invalid_emails = ["yasmin.com", "yasmin@email+host.com"]
        invalid_emails.each do |invalids|
            @buyer.email = invalids
            assert_not @buyer.valid?
        end
    end

    test "buyer should be invalid because cpf is empty" do
        @buyer.cpf = nil
        assert_not @buyer.valid?
    end

    test "buyer should be invalid because cpf is too short" do
        @buyer.cpf = "123"
        assert_not @buyer.valid?
    end

    test "buyer should be invalid because cpf is too long" do
        @buyer.cpf = "1"*15
        assert_not @buyer.valid?
    end
end