require 'test_helper'

class CardTest < ActiveSupport::TestCase
  def setup
    @card = Card.new(holder_name:"Yasmin Dias", number: "1234567878901234", exp_date: "06/21", cvv: "123")
  end

  test "card should be valid" do
    assert @card.valid?
  end

  test "card should be invalid because holder name is empty" do
    @card.holder_name = nil
    assert_not @card.valid?
  end

  test "card should be invalid because holder name is too long" do
    @card.holder_name = "a"*150
    assert_not @card.valid?
  end

  test "card should be invalid because number is empty" do
    @card.number = nil
    assert_not @card.valid?
  end

  test "card should be invalid because number contains letters" do
    @card.number = "12345678ADC3456"
    assert_not @card.valid?
  end

  test "card should be invalid because number is too short" do
    @card.number = "123456789"
    assert_not @card.valid?
  end

  test "card should be invalid because number is too long" do
    @card.number = "123456789789456123456789"
    assert_not @card.valid?
  end

  test "card should be invalid because expiration date is empty" do
    @card.exp_date = nil
    assert_not @card.valid?
  end

  test "card should be invalid because expiration date is in the wrong format" do
    @card.exp_date = "2021/06"
    assert_not @card.valid?
  end

  test "card should be invalid because expiration date is too short" do
    @card.exp_date = "20"
    assert_not @card.valid?
  end

  test "card should be invalid because expiration date is too long" do
    @card.exp_date = "2021/06564"
    assert_not @card.valid?
  end

  test "card should be invalid because cvv is in the wrong format" do
    @card.cvv = "ab1"
    assert_not @card.valid?
  end

  test "card should be invalid because cvv is too short" do
    @card.cvv = "1"
    assert_not @card.valid?
  end

  test "card should be invalid because cvv is too long" do
    @card.cvv = "123456"
    assert_not @card.valid?
  end
end