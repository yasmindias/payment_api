require 'test_helper'

class ClientTest < ActiveSupport::TestCase
  def setup
    @client = Client.new(name: "Yasmin Dias", cpfcnpj: "23213213451")
  end

  test "client should be valid" do
    assert @client.valid?
  end

  test "should be invalid because name is empty" do
    @client.name = nil
    assert_not @client.valid?
  end

  test "should be invalid because name is too long" do
    @client.name = "a"*150
    assert_not @client.valid?
  end

  test "should be invalid because cpfcnppj is empty" do
    @client.cpfcnpj = nil
    assert_not @client.valid?
  end

  test "should be invalid because cpfcnpj has letters" do
    @client.cpfcnpj = "abc123y2309"
    assert_not @client.valid?
  end

  test "should be invalid because cpfcnpj is too short" do
    @client.cpfcnpj = "1"
    assert_not @client.valid?
  end

  test "should be invalid because cpfcnpj is too long" do
    @client.cpfcnpj = "1"*15
    assert_not @client.valid?
  end
end