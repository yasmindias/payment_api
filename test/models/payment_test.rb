require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  def setup
    @payment = Payment.new(amount: 10.00, payment_type: "B", status: "OK")
  end

  test "payment should be valid" do
    assert @payment.valid?
  end

  test "payment shoud be invalid because of amount" do
    @payment.amount = nil
    assert_not @payment.valid?
  end

  test "payment shoud be invalid because of payment_type" do
    @payment.payment_type = "C"
    assert_not @payment.valid?
  end

  test "payment shoud be invalid because of status" do
    @payment.status = nil
    assert_not @payment.valid?
  end
end
