class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.integer :amount
      t.string :payment_type
      t.string :status
      t.string :boleto_number
      t.references :card, foreign_key: true
      t.references :buyer, foreign_key: true

      t.timestamps
    end
  end
end
