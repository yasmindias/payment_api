class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|
      t.string :holder_name
      t.string :number
      t.string :exp_date
      t.string :cvv
      t.string :card_issuer

      t.timestamps
    end
  end
end
