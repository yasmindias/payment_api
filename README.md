# Payment API

This is API was developed as part of the hiring process for Moip Pagamentos's Software Developer position.

## Requirements

The database used for this test is SQLite, to make the install proccess easier, so the only requirement is Rails 5.2.

## Installation
- Download or clone the repository
- Run `bundle install`
- Run `rails s` to start the server

## Usage

### Register Buyer
`POST /buyer`

**All fields are required.**

| Field      | Type      | Description       |
| -----------|:---------:|:------------------|
| name       | string    | Buyer's name      |
| email      | string    | Buyer's email     |
| cpf        | string    | Buyer's CPF       |

##### JSON example
```` json
{
    "name":"John Doe", 
    "email":"johndoe@host.com", 
    "cpf":21321312345
}
````

### Search Buyer
`GET /buyer/:id`

##### Result JSON
```` json
{
    "id": 1,
    "name": "John Doe",
    "email": "johndoe@host.com",
    "cpf": "12345621311",
    "created_at": "2018-10-06T18:40:53.404Z",
    "updated_at": "2018-10-06T18:40:53.404Z"
}
````

### List All Buyers
`GET /buyer/`

##### Result JSON
```` json
[
    {
        "id": 2,
        "name": "Jane Doe",
        "email": "janedoe@host.com",
        "cpf": "09887654312",
        "created_at": "2018-10-07T12:30:53.404Z",
        "updated_at": "2018-10-07T12:30:53.404Z"
    },
    {
        "id": 1,
        "name": "John Doe",
        "email": "johndoe@host.com",
        "cpf": "12345621311",
        "created_at": "2018-10-06T18:40:53.404Z",
        "updated_at": "2018-10-06T18:40:53.404Z"
    }
]
````

### Delete Buyer
`DELETE /buyer/:id`

##### Result JSON
```` json
{
    "message": "Deleted",
    "id": 1
}
````

### Register Client
`POST /client`

**All fields are required.**

| Field      | Type      | Description           |
| -----------|:---------:|:----------------------|
| name       | string    | Client's name         |
| cpfcnpj    | string    | Clients's CPF or CNPJ |

##### JSON example
```` json
{
	"name": "John Doe", 
	"cpfcnpj":"12345678901"
}
```` 

### Search Client
`GET /client/:id`

##### Result JSON
```` json
{
    "id": 1,
    "name": "John Doe",
    "cpfcnpj": "12345678901",
    "created_at": "2018-10-07T16:52:40.938Z",
    "updated_at": "2018-10-07T16:52:40.938Z"
}
````

### List All Clients
`GET /client/`

##### Result JSON
```` json
[
    {
        "id": 2,
        "name": "Jane Doe",
        "cpfcnpj": "09876543210",
        "created_at": "2018-10-07T16:55:36.868Z",
        "updated_at": "2018-10-07T16:55:36.868Z"
    },
    {
        "id": 1,
        "name": "John Doe",
        "cpfcnpj": "12345678901",
        "created_at": "2018-10-07T16:52:40.938Z",
        "updated_at": "2018-10-07T16:52:40.938Z"
    }
]
````

### Delete Client
`DELETE /client/:id`

##### Result JSON
```` json
{
    "message": "Deleted",
    "id": 1
}
````

### Register Payment
`POST /payment/`

**It's necessary to register buyer and card (if the payment type is card) before registering a payment.**

| Field        | Type      | Description                                                  |
| -------------|:---------:|:-------------------------------------------------------------|
| amount       | double    | The amount that will be payed                                |
| payment_type | string    | Card (C) or Boleto (B)                                       |
| buyer_id     | string    | Id of the registered Buyer                                   |
| card_id      | string    | Id of the registered Card (Only if payment type is Card (C)) |

##### JSON example
```` json
{
	"amount":20.00, 
	"payment_type":"C",
	"card_id":1,
	"buyer_id":1
}
```` 

##### Result JSON
If payment type is Card:
```` json
{
    "status": "SUCCESSFUL"
}
```` 

If payment type is Boleto:
```` json
{
    "boleto_number": "1233487327489382987349827438932"
}
```` 

### Search Payment
`GET /payment/:id`

##### Result JSON
```` json
{
    "id": 1,
    "amount": 10,
    "payment_type": "C",
    "status": "SUCCESSFUL",
    "boleto_number": null,
    "card_id": 1,
    "buyer_id": 1,
    "created_at": "2018-10-06T18:58:12.586Z",
    "updated_at": "2018-10-06T18:58:12.586Z"
}
````

### List All Payments
`GET /payment/`

##### Result JSON
```` json
[
    {
        "id": 2,
        "amount": 20,
        "payment_type": "B",
        "status": "SUCCESSFUL",
        "boleto_number": "1233487327489382987349827438932",
        "card_id": 1,
        "buyer_id": 1,
        "created_at": "2018-10-07T17:10:25.642Z",
        "updated_at": "2018-10-07T17:10:25.642Z"
    },
    {
        "id": 1,
        "amount": 20,
        "payment_type": "C",
        "status": "SUCCESSFUL",
        "boleto_number": null,
        "card_id": 1,
        "buyer_id": 1,
        "created_at": "2018-10-07T17:08:41.733Z",
        "updated_at": "2018-10-07T17:08:41.733Z"
    }
]
````

### Delete Payment
`DELETE /payment/:id`

##### Result JSON
```` json
{
    "message": "Deleted",
    "id": 1
}
````

### Checkout
`POST /checkout/`

The object **card_info** is only necessary if the payment type is Card. 
```` json
{
	"buyer_info":{
		"name":"Yasmin Dias", 
		"email":"yasmindias.info@gmail.com",
		"cpf":"12345621311"
	},
	"card_info":{
		"number":4111111111111111
	},
	"payment_info":{
		"payment_type":"C", 
		"amount":10.00
	}
}
````

##### Result JSON
The result of this request is the same as the payment's request.

If payment type is Card:
```` json
{
    "status": "SUCCESSFUL"
}
```` 

If payment type is Boleto:
```` json
{
    "boleto_number": "1233487327489382987349827438932"
}
```` 